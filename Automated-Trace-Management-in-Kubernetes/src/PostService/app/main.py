from flask import Flask, request, jsonify

app = Flask(__name__)

# Mock database for posts (in-memory storage)
posts = []

@app.route('/posts', methods=['POST'])
def create_post():
    post = request.json
    post['id'] = len(posts) + 1  # Simple ID assignment
    posts.append(post)
    return jsonify(post), 201

@app.route('/posts', methods=['GET'])
def get_posts():
    return jsonify(posts)

@app.route('/posts/<int:post_id>', methods=['GET'])
def get_post(post_id):
    post = next((p for p in posts if p['id'] == post_id), None)
    if post:
        return jsonify(post)
    else:
        return jsonify({'error': 'Post not found'}), 404

@app.route('/posts/<int:post_id>', methods=['PUT'])
def update_post(post_id):
    post = next((p for p in posts if p['id'] == post_id), None)
    if post:
        post.update(request.json)
        return jsonify(post)
    else:
        return jsonify({'error': 'Post not found'}), 404

@app.route('/posts/<int:post_id>', methods=['DELETE'])
def delete_post(post_id):
    global posts
    post = next((p for p in posts if p['id'] == post_id), None)
    if post:
        posts = [p for p in posts if p['id'] != post_id]
        return jsonify({'success': 'Post deleted'})
    else:
        return jsonify({'error': 'Post not found'}), 404

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5002, debug=True)
