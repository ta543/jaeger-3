from flask import Flask, request, jsonify

app = Flask(__name__)

# In-memory storage for comments
comments = []

@app.route('/comments', methods=['POST'])
def post_comment():
    comment = request.json
    comments.append(comment)
    return jsonify(comment), 201

@app.route('/comments', methods=['GET'])
def get_comments():
    return jsonify(comments)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
