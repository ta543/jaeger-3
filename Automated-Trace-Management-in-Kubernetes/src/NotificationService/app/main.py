from flask import Flask, request, jsonify

app = Flask(__name__)

# Mock database for notifications (in-memory)
notifications = []

@app.route('/notifications', methods=['POST'])
def create_notification():
    notification = request.json
    notifications.append(notification)
    return jsonify(notification), 201

@app.route('/notifications', methods=['GET'])
def get_notifications():
    return jsonify(notifications)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)
