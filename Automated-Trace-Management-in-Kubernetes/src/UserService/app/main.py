from flask import Flask, request, jsonify

app = Flask(__name__)

# Mock database for users (in-memory storage)
users = []

@app.route('/users', methods=['POST'])
def register_user():
    user = request.json
    user['id'] = len(users) + 1  # Simple ID assignment
    users.append(user)
    return jsonify(user), 201

@app.route('/users', methods=['GET'])
def list_users():
    return jsonify(users)

@app.route('/users/<int:user_id>', methods=['GET'])
def get_user(user_id):
    user = next((u for u in users if u['id'] == user_id), None)
    if user:
        return jsonify(user)
    else:
        return jsonify({'error': 'User not found'}), 404

@app.route('/users/<int:user_id>', methods=['PUT'])
def update_user(user_id):
    user = next((u for u in users if u['id'] == user_id), None)
    if user:
        user.update(request.json)
        return jsonify(user)
    else:
        return jsonify({'error': 'User not found'}), 404

@app.route('/users/<int:user_id>', methods=['DELETE'])
def delete_user(user_id):
    global users
    user = next((u for u in users if u['id'] == user_id), None)
    if user:
        users = [u for u in users if u['id'] != user_id]
        return jsonify({'success': 'User deleted'})
    else:
        return jsonify({'error': 'User not found'}), 404

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5003, debug=True)
