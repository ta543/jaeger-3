#!/bin/bash

# Define the root directory and monitoring directory
ROOT_DIR="$(dirname "$(realpath "$0")")/.."
MONITORING_DIR="$ROOT_DIR/monitoring"

# Deploy Prometheus and Grafana
kubectl apply -f "$MONITORING_DIR/prometheus/prometheus-config.yaml"
kubectl apply -f "$MONITORING_DIR/prometheus/alertmanager-config.yaml"
kubectl apply -f "$MONITORING_DIR/grafana/datasources/"
kubectl apply -f "$MONITORING_DIR/grafana/dashboards/"

echo "Monitoring setup completed successfully."
