#!/bin/bash

# Define the root directory and k8s directory
ROOT_DIR="$(dirname "$(realpath "$0")")/.."
K8S_DIR="$ROOT_DIR/k8s"
MONITORING_DIR="$ROOT_DIR/monitoring"

# Option to also teardown monitoring
while getopts "m" opt; do
  case $opt in
    m)
      echo "Tearing down monitoring setup..."
      kubectl delete -f "$MONITORING_DIR/prometheus/"
      kubectl delete -f "$MONITORING_DIR/grafana/"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done

# Tear down the application
kubectl delete -k "$K8S_DIR/base/"
kubectl delete -k "$K8S_DIR/overlays/production/"

echo "Application and monitoring torn down successfully."
