#!/bin/bash

# Define the root directory and k8s directory
ROOT_DIR="$(dirname "$(realpath "$0")")/.."
K8S_DIR="$ROOT_DIR/k8s"

# Deploy base configuration and selected overlay
kubectl apply -k "$K8S_DIR/base/"
kubectl apply -k "$K8S_DIR/overlays/production/"

echo "Application deployed successfully."
